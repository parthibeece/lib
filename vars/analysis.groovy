def call(sonarhosturl,sonarlogin,sonarprojectKey,sonarprojectName,sonarprojectVersion,sonarsources,sonarlanguage,sonarjavabinaries,sonarjavacoveragePlugin,sonarexclusions,sonarcoveragejacocoxmlReportPaths,sonarsourceEncoding,sonarorganization)
{

            def SCANNER_HOME = tool 'Sonar-Jenkins'
			
              withSonarQubeEnv( credentialsId: 'sonartoken', installationName: 'sonarcloud') {
                  sh "${SCANNER_HOME}/bin/sonar-scanner -Dsonar.host.url=${sonarhosturl} -Dsonar.login=${sonarlogin} -Dsonar.projectKey=${sonarprojectKey} -Dsonar.projectName=${sonarprojectName} -Dsonar.projectVersion=${sonarprojectVersion} -Dsonar.sources=${sonarsources} -Dsonar.language=${sonarlanguage} -Dsonar.java.binaries=${sonarjavabinaries} -Dsonar.java.coveragePlugin=${sonarjavacoveragePlugin} -Dsonar.exclusions=${sonarexclusions} -Dsonar.coverage.jacoco.xmlReportPaths=${sonarcoveragejacocoxmlReportPaths} -Dsonar.sourceEncoding=${sonarsourceEncoding} -Dsonar.organization=${sonarorganization}"
          }

                                  

}

