def call(body)
   {
 
   def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

 def giturllink=config.giturllink ?: ''
 def dockerimage=config.dockerimage ?: ''
 def mavenbuild=config.mavenbuild ?: ''
 def sonarhosturl=config.sonarhosturl ?: ''
 def sonarlogin=config.sonarlogin ?: ''
 def sonarprojectKey=config.sonarprojectKey ?: ''
 def sonarprojectName=config.sonarprojectName ?: ''
 def sonarprojectVersion=config.sonarprojectVersion ?: ''
 def sonarsources=config.sonarsources ?: ''
 def sonarlanguage=config.sonarlanguage ?: ''
 def sonarjavabinaries=config.sonarjavabinaries ?: ''
 def sonarjavacoveragePlugin=config.sonarjavacoveragePlugin ?: ''
 def sonarexclusions=config.sonarexclusions ?: ''
 def sonarcoveragejacocoxmlReportPaths=config.sonarcoveragejacocoxmlReportPaths ?: ''
 def sonarsourceEncoding=config.sonarsourceEncoding ?: ''
 def sonarorganization=config.sonarorganization ?: ''



pipeline {
    agent any
  
    stages {
         stage('CleanUp'){
            steps{
                deleteDir()
            }
        }
        stage('cloning repo') {
            
           steps {
                repo(giturllink)
          }
        }
        
      stage('build app') {        
        
            steps
            {
                dockerbuild(dockerimage,mavenbuild)
               
            }
      }
  /*   stage('SonarQube analysis') {
          steps{
              
               analysis(sonarhosturl,sonarlogin,sonarprojectKey,sonarprojectName,sonarprojectVersion,sonarsources,sonarlanguage,sonarjavabinaries,sonarjavacoveragePlugin,sonarexclusions,sonarcoveragejacocoxmlReportPaths,sonarsourceEncoding,sonarorganization)
         }
        }
          stage("Quality Gate"){
            steps{
                 quality()
         }
        }
        */
      
    }
  }	
}